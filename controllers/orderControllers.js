const Order = require("../models/Order");
const Product = require("../models/Product");
const auth = require("../auth");

module.exports.order = async(req, res) => {
	const token = req.headers.authorization;
	const user = auth.decode(token);

	if(user.isAdmin != true){	

		let productCount = req.body.products.length;
		let products = [];
		

		for(let i = 0; i < productCount; i++){

			let remainingStocks = await Product.findById(req.body.products[i].productId)
			.then(stock => stock.stocks)
			.catch(error => res.send(error));
			//console.log(remainingStocks);

			let updatedStock = {
				stocks: remainingStocks - req.body.products[i].quantity
			}

			if(updatedStock.stocks >= 0){
				products.push({
					productId: req.body.products[i].productId,
					productName: req.body.products[i].productName,
					quantity: req.body.products[i].quantity
				});
				await Product.findByIdAndUpdate(req.body.products[i].productId, updatedStock, {new: true})
				.then(result => res.status(200))
				.catch(error => res.send(error));

				let updatedRemainingStocks = await Product.findById(req.body.products[i].productId)
				.then(stock => stock.stocks)
				.catch(error => res.send(error));

				if(updatedRemainingStocks == 0){
					let outOfStock = {
						isActive: false
					};
					await Product.findByIdAndUpdate(req.body.products[i].productId, outOfStock, {new: true})
					.then(result => res.status(200))
					.catch(error => res.send(error));
				}
			}
			else{
				return res.send(`Remaining Stock for ${req.body.products[i].productName} is ${remainingStocks}`);
			};
			

		};

		let order = new Order({
			products: products,
			userId: user.id,
			totalAmount: req.body.totalAmount
		});
		order.save()
		.then(result => {
			return res.send("Checkout Successful");
		})
		.catch(error => {
			//console.log(error);
			return res.send("Checkout Unsuccessful");
		})
	}else{
		return res.send("Admins cannot order");
	}
};

module.exports.retrieveOrders = (req, res) => {
	let token = req.headers.authorization;
	let user = auth.decode(token);

	if(user.isAdmin){
		return res.send("Admins does not have an order list");
	}else{
		Order.find({userId: user.id})
		.then(orders => {
			return res.send(orders);
		})
		.catch(error => {
			//console.log(error);
			return res.send(error);
		});
	}
};

module.exports.retrieveAllOrders = (req, res) => {
	let token = req.headers.authorization;
	let user = auth.decode(token);

	if(user.isAdmin){
		Order.find({})
		.then(orders => {
			return res.send(orders);
		})
		.catch(error => {
			//console.log(error);
			res.send(error);
		});
	}else{
		return res.send("You don't have access to this page");
	}
};