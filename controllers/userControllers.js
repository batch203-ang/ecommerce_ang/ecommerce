const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");

module.exports.createUser = (req, res) => {
	let newUser = new User({
		firstName: req.body.firstName,
		lastName: req.body.lastName,
		email: req.body.email,
		password: bcrypt.hashSync(req.body.password, 10),
		mobileNo: req.body.mobileNo
	});
	//check if user exists
	User.findOne({email: req.body.email})
	.then(user => {
		if(user != null && user.email == req.body.email){
			return res.send("User with the same email already exists.");
		}else{
			newUser.save()
			.then(savedUser => {
				//console.log(savedUser);
				return res.send("New user created");
			})
			.catch(error => {
				//console.log(error);
				return res.send("Registration failed");
			});
		}
	})
	.catch(error => {
		//console.log(error);
		return res.send(error);
	})
	
};

module.exports.login = (req, res) => {
	return User.findOne({email: req.body.email})
	.then(user => {
		//console.log(user);
		//console.log(user.email);
		if(user != null){
			const isPasswordCorrect = bcrypt.compareSync(req.body.password, user.password);
			if(isPasswordCorrect){
				//console.log(auth.createAccessToken(user));
				return res.send({accessToken: auth.createAccessToken(user)});
			}else{
				//console.log("Incorrect Password");
				return res.send("Incorrect Username or Password");
			};
		}else{
			//console.log("No User Found");
			return res.send("No User Found");
		};
	})
	.catch(error => {
		//console.log(error);
		return res.send(false);
	});
};

module.exports.viewUserDetails = (req, res) => {
	let token = req.headers.authorization;
	let user = auth.decode(token);
	User.findById(user.id)
	.then(user => {
		user.password = "";
		return res.send(user);
	})
	.catch(error => {
		return res.send("User not Found");
	});
};

module.exports.appointAdmin = (req, res) => {
	let token = req.headers.authorization;
	let user = auth.decode(token);

	if(user.isAdmin){
		let setAdmin = {
			isAdmin: req.body.isAdmin
		};
		User.findByIdAndUpdate(req.params.userId, setAdmin, {new: true})
		.then(updatedUser => {
			return res.send(updatedUser);
		})
		.catch(error => {
			//console.log(error);
			return res.send(error);
		})
	}else{
		return res.send("You don't have access to this page");
	}
};