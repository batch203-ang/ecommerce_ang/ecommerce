const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes");
const orderRoutes = require("./routes/orderRoutes");

//express
const port = process.env.PORT|| 4000;
const app = express();

//mongoose
mongoose.connect("mongodb+srv://admin:admin@zuitt-bootcamp.z2lte8d.mongodb.net/ecommerceAPI?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});
let db = mongoose.connection;
db.on("error", console.error.bind(console, "MongoDB connection failed."));
db.once("open", () => console.log("Successfully connected to MongoDB."));

//middlewares
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors());

//links
app.use("/users", userRoutes);
app.use("/products", productRoutes);
app.use("/orders", orderRoutes);

app.listen(port, () => console.log(`Server running at localhost:${port}`));