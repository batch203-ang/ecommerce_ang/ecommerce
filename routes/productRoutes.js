const express = require("express");
const router = express.Router();
const productControllers = require("../controllers/productControllers");
const auth = require("../auth");

router.post("/add", auth.verify, productControllers.createProduct);
router.get("/active", productControllers.viewActiveProducts);
router.get("/all", productControllers.viewAllProducts);
router.get("/:productId", productControllers.viewProduct);
router.put("/edit/:productId", auth.verify, productControllers.updateProduct);
router.patch("/archive/:productId", auth.verify, productControllers.archiveProduct);

module.exports = router;