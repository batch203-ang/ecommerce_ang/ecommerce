const mongoose = require("mongoose");

let orderSchema = new mongoose.Schema({
	totalAmount: {
		type: Number,
		required: [true, "Total Order Amount is required"]
	},
	purchasedOn: {
		type: Date,
		default: new Date()
	},
	userId: {
		type: String,
		required: [true, "User ID is required"]
	},
	products:[{
			productId: {
				type: String,
				required: [true, "Product ID is required"]
			},
			productName: {
				type: String,
				required: [true, "Product Name is required"]
			},
			quantity: {
				type: Number,
				required: [true, "Quantity is required"]
			}
		}]
});

module.exports = mongoose.model("orders", orderSchema);