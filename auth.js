const jwt = require("jsonwebtoken");
const secret = "ecommerceAPI";

module.exports.createAccessToken = (user) => {
	const loggedUser = {
		id: user.id,
		email: user.email,
		isAdmin: user.isAdmin
	};
	return jwt.sign(loggedUser, secret, {});
};

module.exports.verify = (req, res, next) => {
	let token = req.headers.authorization;
	if(token != undefined){
		token = token.slice(7, token.length);
		//console.log(token);

		return jwt.verify(token, secret, (err, res) => {
			if(err){
				//console.log("Verification failed. Invalid Token.");
				return res.send("Verification failed. Invalid Token.");
			}else{
				next();
			}
		});
	}
	else{
		//console.log("No token provided.");
		return res.send("No token provided.");
	}
};

module.exports.decode = (token) => {
	token = token.slice(7, token.length);
	if(token != undefined){
		return jwt.verify(token, secret, (err, res) => {
			if(err){
				return null;
			}
			else{
				return jwt.decode(token, {complete: true}).payload;
			}
		});
	}
	else{
		//console.log("No token provided");
		return null;
	};
};